-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 12, 2019 at 08:01 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `codemead`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `title`, `body`, `created_at`, `updated_at`) VALUES
(1, 'Est iusto debitis rerum sit tempore vel.', 'Laborum ab tempore nostrum repudiandae. Soluta repudiandae similique id nulla voluptatem voluptatem. Et dolor numquam quia. Et ducimus aut fugiat magni ipsum ut.', '2019-03-11 10:52:34', '2019-03-11 10:52:34'),
(2, 'Provident amet perferendis velit.', 'In in totam optio. Odit maiores nemo consequatur nostrum et aperiam officia. Quia veritatis eum est similique id rem nisi.', '2019-03-11 10:52:34', '2019-03-11 10:52:34'),
(3, 'Ut nemo architecto quia sit ut eaque.', 'Aut odit repudiandae mollitia ipsum. Autem et earum nesciunt rerum nisi eum nostrum. Nulla et tenetur optio cum odio sit et id. Voluptas non eum et ex veritatis ut qui.', '2019-03-11 10:52:34', '2019-03-11 10:52:34'),
(4, 'Magni eius inventore numquam.', 'Voluptatem necessitatibus doloribus consequatur maxime. Deleniti velit vero velit tempore nulla quis. Quae dolorum harum aperiam ut. Maxime officiis soluta et ex.', '2019-03-11 10:52:34', '2019-03-11 10:52:34'),
(5, 'Rerum cum expedita hic numquam.', 'Maxime illo et dolores et molestias quo. Dicta et ut rerum optio dignissimos. Consequuntur dolorem esse est vero. Occaecati alias illo culpa ipsam est perferendis.', '2019-03-11 10:52:34', '2019-03-11 10:52:34'),
(6, 'Maxime est hic quidem expedita.', 'Neque non et laborum quo qui. Itaque ut aut delectus et fuga odio ex. Et consequuntur ut et quam aut. Voluptate quaerat optio qui ut ut placeat sint minus. Commodi optio enim harum dolor ducimus et.', '2019-03-11 10:52:35', '2019-03-11 10:52:35'),
(7, 'Beatae ipsum ut odio ut accusamus.', 'Occaecati dolorum aliquid sit officia at est. Magni et ut consequatur mollitia. Aut libero alias vel voluptas occaecati.', '2019-03-11 10:52:35', '2019-03-11 10:52:35'),
(8, 'Soluta quis mollitia dolores autem.', 'Autem enim dicta vero vel aperiam ea dolores voluptas. Deleniti harum maxime rerum non. Quod laborum odit eos aspernatur ut temporibus assumenda minima.', '2019-03-11 10:52:35', '2019-03-11 10:52:35'),
(9, 'Sit eius perferendis iste non omnis similique.', 'Debitis qui quaerat quidem minus mollitia dolorem. Hic veniam vero asperiores labore aperiam sequi harum. Sit quo ut possimus suscipit eum est sapiente. Deserunt ut consectetur possimus sunt in.', '2019-03-11 10:52:35', '2019-03-11 10:52:35'),
(10, 'Et autem optio autem non rerum.', 'Impedit aut asperiores nulla vero est et magni. Corporis sed id iure temporibus quod odio consequuntur. Mollitia eius consequuntur reiciendis ratione est.', '2019-03-11 10:52:35', '2019-03-11 10:52:35'),
(11, 'Tenetur dolor blanditiis itaque nemo.', 'Quas vero unde est recusandae voluptatem. Ut voluptatibus aut molestiae illum. Qui sequi ut ut expedita quia.', '2019-03-11 10:52:35', '2019-03-11 10:52:35'),
(12, 'Et dolorem quo vero iusto et maiores.', 'Corrupti sunt et aliquid perspiciatis distinctio. Dolor sed aliquam vero ut ut et enim facere. Sit aut vero maiores dolore dolor qui. Dolorem voluptates doloribus perspiciatis commodi est in fugit.', '2019-03-11 10:52:35', '2019-03-11 10:52:35'),
(13, 'Ipsam eligendi et et ab aut nulla.', 'Excepturi sapiente ut enim rem ut qui. Quia nihil voluptatem aspernatur distinctio. Sunt error et fuga sed. Fugit doloribus ipsam voluptatem ut in.', '2019-03-11 10:52:35', '2019-03-11 10:52:35'),
(14, 'Facilis ipsum id alias molestiae.', 'Voluptatibus excepturi minus quod exercitationem culpa sit. Officiis autem labore ratione et. Occaecati placeat quidem sint laboriosam est dolore est.', '2019-03-11 10:52:35', '2019-03-11 10:52:35'),
(15, 'Ab culpa dignissimos sed.', 'Perspiciatis reiciendis ut totam optio officiis. Quis quia nisi enim vero velit laudantium distinctio ea.', '2019-03-11 10:52:35', '2019-03-11 10:52:35'),
(16, 'Exercitationem eum optio aliquid molestias.', 'Necessitatibus voluptate dolorem voluptatem qui. Dolorum necessitatibus commodi cum rerum quo. Non amet quae quo.', '2019-03-11 10:52:35', '2019-03-11 10:52:35'),
(17, 'Veritatis eaque non quibusdam mollitia non dolor.', 'Suscipit minima natus dicta eligendi natus. Et enim non fugiat omnis voluptatum possimus at. Amet sit accusantium natus iure doloribus. Ut dolorum possimus sunt.', '2019-03-11 10:52:35', '2019-03-11 10:52:35'),
(18, 'Optio aut velit atque necessitatibus maiores.', 'Earum nihil voluptates impedit quas omnis dolor est ipsa. Aliquid culpa eos natus. Unde iusto laboriosam beatae sit cum et.', '2019-03-11 10:52:35', '2019-03-11 10:52:35'),
(19, 'Saepe et et porro nihil ut ut.', 'Id autem sed inventore ut perferendis. Veritatis numquam aspernatur dolor laborum vel aspernatur sequi. Unde ad ullam quo reprehenderit et est optio quis.', '2019-03-11 10:52:35', '2019-03-11 10:52:35'),
(20, 'Optio ea dolorem vel impedit qui quia at ipsa.', 'Eaque vitae tempora sit omnis aut magni recusandae. Iste dolores ea ut distinctio. Non numquam laboriosam soluta quia est facilis.', '2019-03-11 10:52:35', '2019-03-11 10:52:35'),
(21, 'Sit veniam non animi quaerat.', 'Sed rerum quod voluptatem exercitationem et rerum ex. Deleniti enim ex doloribus. Et architecto vel eum nobis et qui et.', '2019-03-11 10:52:35', '2019-03-11 10:52:35'),
(22, 'Aut recusandae voluptatem est ipsum est eum.', 'Unde dicta veniam libero eaque. Vel aperiam aut cumque harum nihil. Quisquam est asperiores magnam blanditiis maxime beatae id. Necessitatibus tempora ut sed sapiente ducimus eum.', '2019-03-11 10:52:35', '2019-03-11 10:52:35'),
(23, 'Rerum consequuntur omnis aspernatur ab mollitia.', 'Aut quia maiores voluptate. Sint aut qui qui recusandae maxime. Officia iste ducimus voluptates debitis.', '2019-03-11 10:52:35', '2019-03-11 10:52:35'),
(24, 'Et quis doloremque ipsam aut incidunt.', 'Placeat consequatur in non. Sed autem debitis alias praesentium fuga facilis. Facilis maxime reprehenderit et optio corrupti.', '2019-03-11 10:52:35', '2019-03-11 10:52:35'),
(25, 'Quod et mollitia expedita odit ab quasi.', 'Eaque quod accusamus consequatur nostrum rem. Dignissimos et autem maiores eaque hic porro. Dolorem consequatur eos ad placeat aut amet quae. Modi quod et dolor atque ut optio libero.', '2019-03-11 10:52:35', '2019-03-11 10:52:35'),
(26, 'Aut corporis aut animi incidunt nihil.', 'Dolores sequi id asperiores ab quod et ratione. Perferendis culpa libero velit facere. Cum repudiandae recusandae ipsa quisquam voluptatum. Doloribus commodi ut est nisi perferendis enim.', '2019-03-11 10:52:35', '2019-03-11 10:52:35'),
(27, 'Et voluptatem quod officia voluptatem doloremque.', 'Aut voluptatem itaque amet dolore repudiandae in nesciunt. Adipisci consequuntur exercitationem quis rem omnis et eum. Velit asperiores asperiores non sint ipsam.', '2019-03-11 10:52:35', '2019-03-11 10:52:35'),
(28, 'Iste corporis vel commodi quos expedita.', 'Enim est consequatur minus eveniet omnis qui excepturi. Voluptatibus molestias quisquam alias voluptas. Consectetur et molestiae saepe.', '2019-03-11 10:52:35', '2019-03-11 10:52:35'),
(29, 'Qui consequatur tempore et dolore tempora et qui.', 'Fuga nihil eligendi cupiditate et. Dolores facilis sed non impedit molestias autem ratione. Porro dolor aut architecto dignissimos quis cupiditate. Et architecto assumenda autem.', '2019-03-11 10:52:35', '2019-03-11 10:52:35'),
(30, 'Quia distinctio quod earum magnam.', 'Vel blanditiis omnis eum nihil tenetur. Labore qui quidem laborum voluptas quibusdam. Consequatur sed omnis consequuntur corrupti praesentium.', '2019-03-11 10:52:35', '2019-03-11 10:52:35');

-- --------------------------------------------------------

--
-- Table structure for table `audio`
--

CREATE TABLE `audio` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `audio_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `audio_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `audio`
--

INSERT INTO `audio` (`id`, `audio_name`, `audio_type`, `created_at`, `updated_at`) VALUES
(8, 'Sura_1552413566.mp3', 'mp3', '2019-03-12 11:59:26', '2019-03-12 11:59:26');

-- --------------------------------------------------------

--
-- Table structure for table `audio_rating`
--

CREATE TABLE `audio_rating` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `audio_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `comment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_03_08_062351_create_customers_table', 1),
(9, '2019_03_09_101152_create_audio_table', 1),
(10, '2019_03_09_101252_create_videos_table', 1),
(11, '2019_03_09_101329_create_p_d_f_s_table', 1),
(12, '2019_03_09_101508_create_written_posts_table', 1),
(13, '2019_03_09_105959_create_audio_rating_table', 1),
(14, '2019_03_09_110121_create_video_rating_table', 1),
(15, '2019_03_09_110142_create_pdf_rating_table', 1),
(16, '2019_03_09_110159_create_written_post_rating_table', 1),
(17, '2019_03_11_060101_create_articles_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('5c48233f9f1079c75c7919b1d5b0e392ecef4e2fb0245a449a82335f2cd93c6169e051befae5bf64', 1, 1, 'TutsForWeb', '[]', 0, '2019-03-11 10:55:45', '2019-03-11 10:55:45', '2020-03-11 16:55:45'),
('93f43618667aaaef52cca4b11242fb44e665fe0614a2fa4924138fc130130d92c9110f5c396737e0', 1, 1, 'TutsForWeb', '[]', 0, '2019-03-12 10:06:51', '2019-03-12 10:06:51', '2020-03-12 16:06:51'),
('aa06333156c5857d5d695ca90d233f4436a33de966e2e64de828306562e3a01611a4bfbf2f01482c', 1, 1, 'TutsForWeb', '[]', 0, '2019-03-11 11:21:06', '2019-03-11 11:21:06', '2020-03-11 17:21:06');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', '8x6hesr0W6DGGJ780bkWHJwGCXi22WYBh3cYB5pZ', 'http://localhost', 1, 0, 0, '2019-03-11 10:55:34', '2019-03-11 10:55:34'),
(2, NULL, 'Laravel Password Grant Client', 'cgeGIZD2jQV7rgxvyPdo2jBCHLKOpmeqGhX8AiM0', 'http://localhost', 0, 1, 0, '2019-03-11 10:55:34', '2019-03-11 10:55:34');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-03-11 10:55:34', '2019-03-11 10:55:34');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pdf_rating`
--

CREATE TABLE `pdf_rating` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pdf_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `comment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `p_d_f_s`
--

CREATE TABLE `p_d_f_s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pdf_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Riad', 'dev.riadul@gmail.com', NULL, '$2y$10$bItuvGU.BLsk0OW/NYSabeHEQS7fY6/gvkE0L6Q176.lc4KiCAqhm', NULL, '2019-03-11 10:50:37', '2019-03-11 10:50:37');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `video_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `video_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `video_rating`
--

CREATE TABLE `video_rating` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `video_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `comment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `written_posts`
--

CREATE TABLE `written_posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `post_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `written_post_rating`
--

CREATE TABLE `written_post_rating` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `written_post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `comment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `audio`
--
ALTER TABLE `audio`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `audio_rating`
--
ALTER TABLE `audio_rating`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pdf_rating`
--
ALTER TABLE `pdf_rating`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `p_d_f_s`
--
ALTER TABLE `p_d_f_s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `video_rating`
--
ALTER TABLE `video_rating`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `written_posts`
--
ALTER TABLE `written_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `written_post_rating`
--
ALTER TABLE `written_post_rating`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `audio`
--
ALTER TABLE `audio`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `audio_rating`
--
ALTER TABLE `audio_rating`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pdf_rating`
--
ALTER TABLE `pdf_rating`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `p_d_f_s`
--
ALTER TABLE `p_d_f_s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `video_rating`
--
ALTER TABLE `video_rating`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `written_posts`
--
ALTER TABLE `written_posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `written_post_rating`
--
ALTER TABLE `written_post_rating`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
