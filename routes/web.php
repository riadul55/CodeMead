<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('media', 'MediaController');

Route::prefix('Customer/')->group(function (){
    Route::resource('customer', 'CustomerController');
});

Route::prefix('Audio/')->group(function (){
    Route::resource('audio', 'AudioController');
});

Route::prefix('Video/')->group(function (){
    Route::resource('video', 'VideoController');
});

Route::prefix('PDF/')->group(function (){
    Route::resource('pdf', 'PDFController');
});

Route::prefix('Written/')->group(function (){
    Route::resource('written', 'WrittenPostController');
});