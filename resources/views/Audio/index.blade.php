@extends('layouts.main')

@section('title')
    Audio
@endsection

@section('content')
    <div class="content-wrapper">

        <section class="content">
            <div class="row" style="margin-left: 10px; margin-right: 10px; margin-top: 10px; margin-bottom: 0px">

                <div class="col-md-2"></div>
                <div class="col-md-8">

                    @include('message')

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Upload Audio File</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">


                                {!! Form::open(['route'=>'audio.store', 'method'=>'post', 'files'=>true ]) !!}
                                {{ Form::token() }}

                                    <div class="form-group">
                                        <div class="row">

                                            <div class="col-md-6">
                                                {!! Form::label('audio_name', 'Audio Name') !!}
                                            </div>
                                            <div class="col-md-6">
                                                {{ Form::text('audio_name','', array('class'=>'form-control-sm', 'required')) }}
                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">

                                            <div class="col-md-6">
                                                {!! Form::label('audio', 'Upload Audio') !!}
                                            </div>
                                            <div class="col-md-6">
                                                {{ Form::file('audio', ['accept' =>  'audio/mp3', 'required']) }}
                                            </div>

                                        </div>
                                    </div>

                                    {{ Form::submit('Upload', array('class' => 'btn btn-primary btn-block btn-sm', 'style' => 'margin-top: 20px'))}}

                                {!! Form::close() !!}


                                <hr>


                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Audio Name</th>
                                        <th>Type</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>

                                    </tr>
                                    </thead>

                                    <tbody>

                                    @foreach($audio_list as $value)
                                        <tr>
                                            <td>{{$value -> audio_name}}</td>
                                            <td>{{$value -> audio_type}}</td>

                                            <td>
                                                <a href="{{route('audio.show', $value->id) }}" class="btn btn-sm btn-warning"><i class="fa fa-bars"></i></a>
                                            </td>
                                            <td>
                                                <a href="{{route('audio.edit', $value->id) }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
                                            </td>
                                            <td>
                                                {!!Form::open(['action' => ['AudioController@destroy', $value->id], 'method' => 'POST', 'onsubmit'=>"if(!confirm('Are You Sure want to DELETE??')){return false;}"])!!}
                                                {{Form::hidden('_method', 'DELETE')}}
                                                <button type="submit" class="btn btn-sm btn-danger" > <i class="fa fa-trash"></i></button>
                                                {!!Form::close()!!}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>


                        </div>
                        <!-- /.box-body -->
                    </div>

                </div>

            </div>
        </section>

    </div>
@endsection
