@extends('layouts.main')

@section('title')
    Audio
@endsection

@section('content')
    <div class="content-wrapper">

        <section class="content">
            <div class="row" style="margin-left: 10px; margin-right: 10px; margin-top: 10px; margin-bottom: 0px">

                <div class="col-md-2"></div>
                <div class="col-md-8">

                    @include('message')

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Update Audio File</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">


                            {!! Form::open(['action' => ['AudioController@update', $audio->id], 'method' => 'PUT', 'files'=>true]) !!}
                            {{ Form::token() }}

                            <div class="form-group">
                                <div class="row">

                                    <div class="col-md-6">
                                        {!! Form::label('audio_name', 'Audio Name') !!}
                                    </div>
                                    <div class="col-md-6">
                                        {{ Form::text('audio_name',$audio->audio_name, array('class'=>'form-control-sm', 'required')) }}
                                    </div>

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">

                                    <div class="col-md-6">
                                        {!! Form::label('audio', 'Upload Audio') !!}
                                    </div>
                                    <div class="col-md-6">
                                        {{ Form::file('audio', ['accept' =>  'audio/mp3', 'required']) }}
                                    </div>

                                </div>
                            </div>

{{--                            {{Form::hidden('_method','PUT')}}--}}
                            {{ Form::submit('Update', array('class' => 'btn btn-primary btn-block btn-sm', 'style' => 'margin-top: 20px'))}}

                            {!! Form::close() !!}


                        </div>
                        <!-- /.box-body -->
                    </div>

                </div>

            </div>
        </section>

    </div>
@endsection
