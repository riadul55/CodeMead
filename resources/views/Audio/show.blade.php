
@extends('Layouts.main')

@section('title')
    Audio
@endsection

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-2"></div>
                <div class="col-md-8">

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title"> Audio Files</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            File Name: <strong> {{$audio->audio_name}}</strong> <br>
                            Extension: <strong> {{$audio->audio_type}}</strong> <br>

                        </div>
                        <!-- /.box-body -->
                    </div>

                </div>

            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->

    </div>
    <!-- /.content-wrapper -->



@endsection
