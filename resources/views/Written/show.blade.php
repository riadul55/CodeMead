
@extends('Layouts.main')

@section('title')
    Post
@endsection

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-2"></div>
                <div class="col-md-8">

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title"> POST Show</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            POST Name: <strong> {{$post->post_name}}</strong> <br>
                            POST Title: <strong> {{$post->post_title}}</strong> <br>
                            <hr>
                            <strong>Body:</strong>{!! $post->body !!} <br>

                        </div>
                        <!-- /.box-body -->
                    </div>

                </div>

            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->

    </div>
    <!-- /.content-wrapper -->



@endsection
