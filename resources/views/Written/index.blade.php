@extends('layouts.main')

@section('title')
    Customer
@endsection

@section('content')
    <div class="content-wrapper">

        <section class="content">
            <div class="row" style="margin-left: 10px; margin-right: 10px; margin-top: 10px; margin-bottom: 0px">

                <div class="col-md-2"></div>

                <div class="col-md-8">

                    @include('message')

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">POST
                                <small>Your Story</small>
                            </h3>
                            <!-- tools box -->
                            <div class="pull-right box-tools">
                                <button type="button" class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip"
                                        title="Collapse">
                                    <i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-default btn-sm" data-widget="remove" data-toggle="tooltip"
                                        title="Remove">
                                    <i class="fa fa-times"></i></button>
                            </div>
                            <!-- /. tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body pad">
                            <form action="{{route('written.store')}}" method="post">
                                @csrf
                                <input type="text" class="form-control form-control-sm" name="post_name" placeholder="Post Name" style="margin-bottom: 5px;" required>
                                <input type="text" class="form-control form-control-sm" name="post_title" placeholder="Post Title" style="margin-bottom: 15px" required>
                                <textarea class="textarea" placeholder="Body..." name="body" required
                                style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                <input type="submit" class="btn btn-block btn-sm btn-success">
                            </form>

                            <hr>

                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>POST Name</th>
                                    <th>POST Title</th>

                                    <th></th>
                                    <th></th>
                                    <th></th>

                                </tr>
                                </thead>

                                <tbody>

                                @foreach($post_list as $value)
                                    <tr>
                                        <td>{{$value -> post_name}}</td>
                                        <td>{{$value -> post_title}}</td>

                                        <td>
                                            <a href="{{route('written.show', $value->id) }}" class="btn btn-sm btn-warning"><i class="fa fa-bars"></i></a>
                                        </td>
                                        <td>
                                            <a href="{{route('written.edit', $value->id) }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
                                        </td>
                                        <td>
                                            {!!Form::open(['action' => ['WrittenPostController@destroy', $value->id], 'method' => 'POST', 'onsubmit'=>"if(!confirm('Are You Sure want to DELETE??')){return false;}"])!!}
                                            {{Form::hidden('_method', 'DELETE')}}
                                            <button type="submit" class="btn btn-sm btn-danger" > <i class="fa fa-trash"></i></button>
                                            {!!Form::close()!!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>



                    </div>


                </div>

            </div>
        </section>

    </div>
@endsection
