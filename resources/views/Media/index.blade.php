@extends('layouts.main')

@section('title')
    Media
@endsection

@section('content')
    <div class="content-wrapper">

        <section class="content">
            <div class="row" style="margin-left: 10px; margin-right: 10px; margin-top: 10px; margin-bottom: 0px">

                <div class="col-md-2"></div>
                <div class="col-md-8">

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Choose MEDIA</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">

                            <form action="{{route('media.store')}}" method="post">
                                @csrf

                                <div class="form-group">
                                    <label>
                                        <input type="radio" name="file" class="flat-red" value="audio">
                                        Audio
                                    </label>
                                    <br>
                                    <label>
                                        <input type="radio" name="file" class="flat-red" value="video">
                                        Video
                                    </label>
                                    <br>
                                    <label>
                                        <input type="radio" name="file" class="flat-red" value="pdf">
                                        PDF
                                    </label>
                                    <br>
                                    <label>
                                        <input type="radio" name="file" class="flat-red" value="written">
                                        Written
                                    </label>
                                </div>


                                <button type="submit" class="btn btn-sm btn-block btn-info pull-right">Next</button>

                            </form>

                        </div>
                        <!-- /.box-body -->
                    </div>

                </div>

            </div>
        </section>

    </div>
@endsection
