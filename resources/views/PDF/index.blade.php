@extends('layouts.main')

@section('title')
    PDF
@endsection

@section('content')
    <div class="content-wrapper">

        <section class="content">
            <div class="row" style="margin-left: 10px; margin-right: 10px; margin-top: 10px; margin-bottom: 0px">

                <div class="col-md-2"></div>

                <div class="col-md-8">

                    @include('message')

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">PDF Upload</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">

                            {!! Form::open(['route'=>'pdf.store', 'method'=>'post', 'files'=>true ]) !!}
                            {{ Form::token() }}

                            <div class="form-group">
                                <div class="row">

                                    <div class="col-md-6">
                                        {!! Form::label('pdf_name', 'PDF Name') !!}
                                    </div>
                                    <div class="col-md-6">
                                        {{ Form::text('pdf_name','', array('class'=>'form-control-sm', 'required')) }}
                                    </div>

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">

                                    <div class="col-md-6">
                                        {!! Form::label('pdf', 'Upload PDF') !!}
                                    </div>
                                    <div class="col-md-6">
                                        {{ Form::file('pdf', ['accept' =>  '.pdf', 'required']) }}
                                    </div>

                                </div>
                            </div>

                            {{ Form::submit('Upload', array('class' => 'btn btn-primary btn-block btn-sm', 'style' => 'margin-top: 20px'))}}

                            {!! Form::close() !!}

                            <hr>

                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>PDF Name</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>

                                </tr>
                                </thead>

                                <tbody>

                                @foreach($pdf_list as $value)
                                    <tr>
                                        <td>{{$value -> pdf_name}}</td>

                                        <td>
                                            <a href="{{route('pdf.show', $value->id) }}" class="btn btn-sm btn-warning"><i class="fa fa-bars"></i></a>
                                        </td>
                                        <td>
                                            <a href="{{route('pdf.edit', $value->id) }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
                                        </td>
                                        <td>
                                            {!!Form::open(['action' => ['PDFController@destroy', $value->id], 'method' => 'POST', 'onsubmit'=>"if(!confirm('Are You Sure want to DELETE??')){return false;}"])!!}
                                            {{Form::hidden('_method', 'DELETE')}}
                                            <button type="submit" class="btn btn-sm btn-danger" > <i class="fa fa-trash"></i></button>
                                            {!!Form::close()!!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>

                </div>

            </div>
        </section>

    </div>
@endsection
