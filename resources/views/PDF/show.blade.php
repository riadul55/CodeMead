
@extends('Layouts.main')

@section('title')
    Customer
@endsection

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-2"></div>
                <div class="col-md-8">

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title"> Customer Information</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            First Name: <strong> {{$customer->first_name}}</strong> <br>
                            Last Name: <strong> {{$customer->last_name}}</strong> <br>
                            Email: <strong> {{$customer->email}}</strong><br>
                            Birthday: <strong> {{$customer->birthday}}</strong><br>
                            Gender: <strong> {{$customer->gender}}</strong><br>
                        </div>
                        <!-- /.box-body -->
                    </div>

                </div>

            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->

    </div>
    <!-- /.content-wrapper -->



@endsection
