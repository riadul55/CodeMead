@extends('layouts.main')

@section('title')
    Customer
@endsection

@section('content')
    <div class="content-wrapper">

        <section class="content">
            <div class="row" style="margin-left: 10px; margin-right: 10px; margin-top: 10px; margin-bottom: 0px">

                <div class="col-md-2"></div>
                <div class="col-md-8">

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Customer Info</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>

                                </tr>
                                </thead>

                                <tbody>

                                @foreach($customer_list as $value)
                                    <tr>
                                        <td>{{$value -> first_name}}</td>
                                        <td>{{$value -> last_name}}</td>
                                        <td>{{$value -> email}}</td>

                                        <td>
                                            <a href="{{route('customer.show', $value->id) }}" class="btn btn-sm btn-warning"><i class="fa fa-bars"></i></a>
                                        </td>
                                        <td>
                                            <a href="{{route('customer.edit', $value->id) }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
                                        </td>
                                        <td>
                                            {!!Form::open(['action' => ['CustomerController@destroy', $value->id], 'method' => 'POST', 'onsubmit'=>"if(!confirm('Are You Sure want to DELETE??')){return false;}"])!!}
                                            {{Form::hidden('_method', 'DELETE')}}
                                            <button type="submit" class="btn btn-sm btn-danger" > <i class="fa fa-trash"></i></button>
                                            {!!Form::close()!!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>

                </div>

            </div>
        </section>

    </div>
@endsection
