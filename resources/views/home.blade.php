@extends('layouts.main')

@section('title')
    HOME
@endsection

@section('content')
<div class="content-wrapper">

<section class="content">
      <div class="row" style="margin-left: 10px; margin-right: 10px; margin-top: 10px; margin-bottom: 0px">
          <div class="col-lg-3 col-xs-12" style="padding-bottom: 0px;">
            <!-- small box -->
            <div class="small-box bg-aqua">
              <div class="inner">
                <h3>{{$audio}}</h3>

                <p>Audio File</p>
              </div>
              <div class="icon">
                <i class="fa fa-file-audio-o"></i>
              </div>
              <a href="{{route('audio.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>

          <div class="col-lg-3 col-xs-12" style="padding-bottom: 0px;">
            <!-- small box -->
            <div class="small-box bg-green">
              <div class="inner">
                <h3>{{$video}}</h3>

                <p>Video File</p>
              </div>
              <div class="icon">
                <i class="fa fa-file-video-o"></i>
              </div>
              <a href="{{route('video.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>

          <div class="col-lg-3 col-xs-12" style="padding-bottom: 0px;">
            <!-- small box -->
            <div class="small-box bg-green">
              <div class="inner">
                <h3>{{$pdf}}</h3>

                <p>PDF</p>
              </div>
              <div class="icon">
                <i class="fa  fa-file-pdf-o"></i>
              </div>
              <a href="{{route('pdf.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>

          <div class="col-lg-3 col-xs-12" style="padding-bottom: 0px;">
            <!-- small box -->
            <div class="small-box bg-aqua">
              <div class="inner">
                <h3>{{$written}}</h3>

                <p>Handwriting</p>
              </div>
              <div class="icon">
                <i class="fa fa-hand-paper-o"></i>
              </div>
              <a href="{{route('written.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
      </div>
</section>

</div>
@endsection
