 <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">

          <li class="header">MAIN NAVIGATION</li>

          <li>
              <a href="{{route('home')}}">
                  <i class="fa fa-dashboard"></i> <span> Dashboard</span>
              </a>
          </li>

          <li>
              <a href="{{route('customer.index')}}">
                  <i class="fa fa-th"></i> <span> Customer</span>
              </a>
          </li>



          <li>
              <a href="{{route('media.index')}}">
                  <i class="fa fa-th"></i> <span> Add MEDIA</span>
              </a>
          </li>

          <!-- <li class="treeview">
              <a href="#">
                  <i class="fa fa-files-o"></i>
                  <span>Set Category</span>
                  <span class="pull-right-container">
                  <span class="label label-primary pull-right">2</span>
                </span>
              </a>
              <ul class="treeview-menu">
                  <li><a href=""><i class="fa fa-circle-o"></i> Add Category</a></li>
                  <li><a href=""><i class="fa fa-circle-o"></i> Add Product to Category</a></li>
              </ul>
          </li> -->
   
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
