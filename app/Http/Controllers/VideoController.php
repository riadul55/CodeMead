<?php

namespace App\Http\Controllers;

use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $video_list = DB::table('videos')->get();
        return view('Video/index')->with('video_list', $video_list);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('video')){

            $file = $request->file('video');

            $filename = $request->input('video_name').'_'.time().'.'.$file->getClientOriginalExtension();
            $path = public_path().'/Video/';
            $file->move($path, $filename);

            DB::table('videos')
                ->insert([
                    'video_name' => $filename,
                    'video_type' => $file->getClientOriginalExtension(),
                    "created_at" =>  \Carbon\Carbon::now(),
                    "updated_at" => \Carbon\Carbon::now()
                ])
            ;
            $video_list = DB::table('videos')->get();
            return view('Video/index')
                ->with('video_list', $video_list)
                ->with('success', 'Video Uploaded')
                ;

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd('ops...');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('videos')->where('id', $id)->delete();
        return redirect('Video/Video')->with('success', 'Video Removed');
    }
}
