<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $audio_list = DB::table('audio')->get()->count();
        $video_list = DB::table('videos')->get()->count();
        $pdf_list = DB::table('p_d_f_s')->get()->count();
        $written_list = DB::table('written_posts')->get()->count();

        return view('home')
            ->with('audio', $audio_list)
            ->with('video', $video_list)
            ->with('pdf', $pdf_list)
            ->with('written', $written_list)
        ;
    }
}
