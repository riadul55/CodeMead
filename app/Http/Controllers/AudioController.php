<?php

namespace App\Http\Controllers;

use App\Audio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class AudioController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $audio_list = DB::table('audio')->get();
        return view('Audio/index')->with('audio_list', $audio_list);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'audio_name' => 'required|unique:audio'
        ]);


        if($request->hasFile('audio')){

            $file = $request->file('audio');

            $filename = $request->input('audio_name').'_'.time().'.'.$file->getClientOriginalExtension();
            $path = public_path().'/Audio/';
            $file->move($path, $filename);

            DB::table('audio')
                ->insert([
                    'audio_name' => $filename,
                    'audio_type' => $file->getClientOriginalExtension(),
                    "created_at" =>  \Carbon\Carbon::now(),
                    "updated_at" => \Carbon\Carbon::now()
                ])
            ;
            $audio_list = DB::table('audio')->get();
            return redirect('Audio/audio')
                ->with('audio_list', $audio_list)
                ->with('success', 'Audio Added!')
            ;

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Audio  $audio
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $audio = DB::table('audio')->find($id);
        return view('Audio.show')
        ->with('audio', $audio)
        ;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Audio  $audio
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $audio = DB::table('audio')->find($id);
        return view('Audio.edit')->with('audio', $audio);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Audio  $audio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'audio_name' => 'required'
        ]);


        if($request->hasFile('audio')){

            $file = $request->file('audio');

            $filename = $request->input('audio_name').'_'.time().'.'.$file->getClientOriginalExtension();
            $path = public_path().'/Audio/';
            $file->move($path, $filename);

            DB::table('audio')->where('id', $id)
                ->update([
                    'audio_name' => $filename,
                    'audio_type' => $file->getClientOriginalExtension(),
                    "updated_at" => \Carbon\Carbon::now()
                ])
            ;

            return redirect('Audio/audio')->with('success', 'Audio Updated !!' );

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Audio  $audio
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('audio')->where('id', $id)->delete();
        return redirect('Audio/audio')->with('success', 'Audio Removed');
    }
}
