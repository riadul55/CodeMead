<?php

namespace App\Http\Controllers;

use App\WrittenPost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WrittenPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post_list = DB::table('written_posts')->get();
        return view('Written/index')->with('post_list', $post_list);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'post_name' => 'required',
            'post_title' => 'required|unique:written_posts',
            'body' => 'required'
        ]);

        $post_name = $request->input('post_name');
        $post_title = $request->input('post_title');
        $body = $request->input('body');

        DB::table('written_posts')
            ->insert([
                'post_name' => $post_name,
                'post_title' => $post_title,
                'body' => $body,
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now()
            ])
        ;
        $post_list = DB::table('written_posts')->get();
        return redirect('Written/written')
            ->with('post_list', $post_list)
            ->with('success', 'Posted!')
            ;


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WrittenPost  $writtenPost
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = DB::table('written_posts')->find($id);
        return view('Written.show')
            ->with('post', $post)
            ;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WrittenPost  $writtenPost
     * @return \Illuminate\Http\Response
     */
    public function edit(WrittenPost $writtenPost)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WrittenPost  $writtenPost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WrittenPost $writtenPost)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WrittenPost  $writtenPost
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('written_posts')->where('id', $id)->delete();
        return redirect('Written/written')->with('success', 'POST Removed');
    }
}
