<?php

namespace App\Http\Controllers;

use App\PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PDFController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pdf_list = DB::table('p_d_f_s')->get();
        return view('PDF/index')->with('pdf_list', $pdf_list);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'pdf_name' => 'required|unique:p_d_f_s'
        ]);

        if($request->hasFile('pdf')){

            $file = $request->file('pdf');

            $filename = $request->input('pdf_name').'_'.time().'.'.$file->getClientOriginalExtension();
            $path = public_path().'/PDF/';
            $file->move($path, $filename);

            DB::table('p_d_f_s')
                ->insert([
                    'pdf_name' => $filename,
                    "created_at" =>  \Carbon\Carbon::now(),
                    "updated_at" => \Carbon\Carbon::now()
                ])
            ;

            return redirect('PDF/pdf')
                ->with('success', 'PDF File Added!')
            ;

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PDF  $pDF
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PDF  $pDF
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PDF  $pDF
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PDF  $pDF
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('p_d_f_s')->where('id', $id)->delete();
        return redirect('PDF/pdf')->with('success', 'PDF Removed');
    }
}
